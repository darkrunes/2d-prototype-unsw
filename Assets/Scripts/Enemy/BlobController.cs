﻿using UnityEngine;
using System.Collections;

public class BlobController : EnemyBase {

    public Transform targetAhead;
    public LayerMask myLayerMask;
    public Transform castOrigin;
    public float speed;

    private Vector2 startPos;
  //  private Rigidbody2D ctrlRigidBody;

    readonly float maxHealth = 30f;
    
    void Start()
    {
        anim = GetComponent<Animator>();
      //  ctrlRigidBody = GetComponent<Rigidbody2D>();
        startPos = transform.position;
        health = maxHealth;
    }

    void FixedUpdate()
    {
        Move();
    }

    public override void Move()
    {
        RaycastHit2D rayCastHit = Physics2D.Raycast(castOrigin.position, targetAhead.position, 1, myLayerMask);

        if (rayCastHit == true)
        {
            Flip();
        }

        transform.position = Vector3.MoveTowards(transform.position, targetAhead.position, Time.deltaTime * speed);
         
    }

    public override void takeDamage(int damageAmount)
    {
        health -= damageAmount;
        if (health <= 0)
        {
            health = 0;
            Die();
        }
    }

    public override void Die()
    {

        //anim.SetBool("Death", true);
        Instantiate(particleSys, transform.position, Quaternion.identity);
        Destroy(gameObject);
        // Replace with activity set, and uses interfaces for it
        // This will help with respawning if player dies
    }
}
