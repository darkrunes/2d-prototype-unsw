﻿using UnityEngine;
using System.Collections;

public abstract class EnemyBase : MonoBehaviour, IDamageable {

    public float health { get; protected set; }
    public Transform particleSys;
    protected Animator anim;

    abstract public void Move();

    abstract public void takeDamage(int damageAmount);

    abstract public void Die();

    protected virtual void OnCollisionEnter2D(Collision2D other)
    {
        var tag = other.collider.tag;

        if (tag == "Enemy")
        {
            Flip();
        }
    }

    protected virtual void Flip()
    {
        var newScale = transform.localScale;

        newScale.x *= -1;
        transform.localScale = newScale;
    }
}
