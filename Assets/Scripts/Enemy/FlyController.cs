﻿using UnityEngine;
using System;
using System.Collections;

public class FlyController : EnemyBase, IDamageable {

    readonly float maxHealth = 20f;

    private float currentPos;
    private float lastPos;
    private int direction;
    private FollowPath folPathScript;
    private Rigidbody2D rigidBody;
    private CircleCollider2D circColl;


    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        folPathScript = GetComponent<FollowPath>();
        anim = GetComponent<Animator>();
        circColl = GetComponent<CircleCollider2D>();
        lastPos = currentPos = transform.position.x;
        direction = -1;
        health = maxHealth;
    }

    void FixedUpdate()
    {
        Move();
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {
        return;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var tag = other.tag;
        if (tag == "Player") 
        {
            other.SendMessage("takeDamage", 10f);
        }
    }

    public override void Move()
    {
        currentPos = transform.position.x;

       // Debug.Log("Current Pos: " + currentPos + "| Last Pos: " + lastPos + " | Direction: " + direction);

        if ((lastPos - currentPos) < 0 && direction == -1)
        {
            Flip();
            direction *= -1;
        }
        else if ((lastPos - currentPos) > 0 && direction == 1)
        {
            Flip();
            direction *= -1;
        }

        lastPos = currentPos;
    }

    public override void takeDamage(int damageAmount)
    {
        health -= damageAmount * 0.5f;

        if (health <= 0)
        {
            health = 0;
            Die();
        }
    }

    public override void Die()
    {
        anim.SetBool("isDead", true);
        rigidBody.isKinematic = false;
        circColl.isTrigger = false;
        folPathScript.enabled = false;

        StartCoroutine(DisableInSeconds(2));
    }

    IEnumerator DisableInSeconds(int timeToWait)
    {
        Debug.Log("Waiting 5 Sec");
        yield return new WaitForSeconds(timeToWait);
        gameObject.SetActive(false);
    }

    
}
