﻿using UnityEngine;
using System.Collections;

public class ParticleManager : MonoBehaviour {

    public float deathDuration;

    void Start()
    {
        Destroy(gameObject, deathDuration);
    }
}
