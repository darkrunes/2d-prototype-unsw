﻿interface IDamageable
{
    float health { get; }

    void takeDamage (int damageAmount);
}