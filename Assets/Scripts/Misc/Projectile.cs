﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{

    void Start()
    {
        Destroy(gameObject, 2);
    }

    public void OnTriggerEnter2D(Collider2D other) 
    {
        Destroy(gameObject);
    }
}
