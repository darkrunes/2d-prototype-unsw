﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowPath : MonoBehaviour
{

    public enum FollowType
    {
        MoveTowards,
        Lerp,
        Slerp
    }

    public FollowType Type = FollowType.MoveTowards;
    public float Speed = 1f;
    public float MaxDistanceToPoint = .1f;

    private PathDefinition Path;
    private IEnumerator<Transform> currentPoint;

    public void Start()
    {
        Path = GetComponent<PathDefinition>();

        if (Path == null)
        {
            Debug.LogError("Path is null", gameObject);
            return;
        }
        currentPoint = Path.GetPathEnumerator();
        currentPoint.MoveNext();

        if (currentPoint.Current == null)
            return;
    }

    public void Update()
    {
        if (currentPoint == null || currentPoint.Current == null)
            return;

        if (Type == FollowType.MoveTowards)
            transform.position = Vector3.MoveTowards(transform.position,
                currentPoint.Current.position, Time.deltaTime * Speed);

        else if (Type == FollowType.Lerp)
            transform.position = Vector3.Lerp(transform.position,
                currentPoint.Current.position, Time.deltaTime * Speed);

        else if (Type == FollowType.Slerp)
            transform.position = Vector3.Slerp(transform.position,
                currentPoint.Current.position, Time.deltaTime * Speed);

        float distanceSquared = (transform.position - currentPoint.Current.position).sqrMagnitude;

        if (distanceSquared < Mathf.Pow(MaxDistanceToPoint, 2))
            currentPoint.MoveNext();
    }


}
