﻿using UnityEngine;
using System.Collections;

public class ProjectileAttack : MonoBehaviour 
{
    public int damageOnHit;
    public int speed;
    public LayerMask hitMask;

    private Rigidbody2D rigidBody;
    private bool isRight { get ;  set; }
    
	void Start () 
    {
        Destroy(gameObject, 2f);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        var col = other.GetComponent<Collider2D>();

        if (other.tag == "Enemy") 
        {
            col.SendMessage("takeDamage", damageOnHit);
        }

        Destroy(gameObject);
    }
}
