﻿using UnityEngine;
using System.Collections;

public class PlatformHandler : MonoBehaviour 
{

    public struct Data
    {
        public CharacterController ctrl;
        public Transform t;
        public float yOffset;

        public Data(CharacterController ctrl, Transform t, float yOffset)
        {
            this.ctrl = ctrl;
            this.t = t;
            this.yOffset = yOffset;
        }
    }
    public float verticalOffset = 0.25f;

    private Hashtable onPlatform = new Hashtable();
    private Vector3 lastPos;

    public void Start()
    {
        lastPos = transform.position;
    }

    public void Update()
    {
        Vector3 curPos = transform.position;
        float y = curPos.y;
        Vector3 delta = curPos - lastPos;
        float yVelocity = delta.y;

        delta.y = delta.x = 0;
        lastPos = curPos;

        foreach (DictionaryEntry d in onPlatform)
        {
            Data data = (Data)d.Value;
            float charYVelocity = data.ctrl.velocity.y;

            if ((charYVelocity == 0f) || (charYVelocity == yVelocity))
            {
                Vector3 pos = data.t.position;
                pos.y = y + data.yOffset;
                pos += delta;
                data.t.position = pos;
            }
        }
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        CharacterController ctrl = other.GetComponent(typeof(CharacterController)) as CharacterController;

        if (ctrl == null)
            return;

        Transform t = other.transform;
        float yOffset = ctrl.height / 2f - ctrl.center.y + verticalOffset;

        Data data = new Data(ctrl, t, yOffset);
        onPlatform.Add(other.transform, data);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        onPlatform.Remove(other.transform);
    }
}
