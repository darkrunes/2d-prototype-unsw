﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    void Update()
    {
        if (Input.GetButtonDown("Restart"))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        other.collider.SendMessage("takeDamage", int.MaxValue);
    }
}
