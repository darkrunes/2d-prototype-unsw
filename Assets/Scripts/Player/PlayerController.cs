﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour, IDamageable
{

    public bool facingRight { get; private set; }
    private bool jumping = false;
    private Transform groundCheck;
    private bool isOnGround;
    private Animator anim;
    private Rigidbody2D playerRigidBody;
    private float fireFrequency;
    private float lastShot;
    private float projSpeed = 15f;

    public float shootForce { get; private set; }
    public float maxSpeed;
    public float jumpForce;
    public Transform projSpawnLocation;
    public Rigidbody2D projectile;
    public float health { get; private set; }

	void Start ()
    {
        facingRight = true;
        health = 100;
        groundCheck = transform.FindChild("Ground Check");
        anim = GetComponent<Animator>();
        playerRigidBody = GetComponent<Rigidbody2D>();
	}
	
	void Update ()
    {
        isOnGround = Physics2D.OverlapCircle(groundCheck.position, 0.01f, LayerMask.GetMask("Ground", "Platform"));

        if (Input.GetButtonDown("Jump") && isOnGround)
            jumping = true;
	}

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        moveHorizontal(horizontal);
        moveVertically(vertical);

        if (Input.GetButtonDown("Shoot"))// && (fireFrequency  <=  lastShot))
        {
            Shoot();
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Platform")
        {
            gameObject.transform.parent = other.gameObject.transform;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Platform")
        {
            gameObject.transform.parent = null;
        }
    }

    private void Shoot()
    {
        var projInstance = Instantiate(projectile, projSpawnLocation.position, Quaternion.identity) as Rigidbody2D;
        anim.SetTrigger("isAttacking");

        if (facingRight)
            projInstance.velocity = new Vector2(projSpeed, 0f);
        else
            projInstance.velocity = new Vector2(-projSpeed, 0f);
         
    }

    private void moveHorizontal(float direction)
    {
        if (playerRigidBody.velocity.x != 0)
            anim.SetBool("isMoving", true);
        else
            anim.SetBool("isMoving", false);

        if (!anim.GetBool("isCrouching"))
            playerRigidBody.velocity = new Vector2(maxSpeed * direction, playerRigidBody.velocity.y);

        if (direction > 0 && !facingRight)
            Flip();
        else if (direction < 0 && facingRight)
            Flip();
    }

    private void moveVertically(float direction)
    {
        if (!isOnGround)
            anim.SetBool("isFalling", true);
        else
        {
            anim.SetBool("isFalling", false);
        }

        if (jumping)
        {
            playerRigidBody.AddForce(new Vector2(0f, jumpForce));
            jumping = false;
        }

        if (direction < 0)
            anim.SetBool("isCrouching", true);
        else
            anim.SetBool("isCrouching", false);
    }

    private void Flip()
    {
        var newScale = transform.localScale;
        newScale.x *= -1;

        facingRight = !facingRight;

        transform.localScale = newScale;
    }

    public void takeDamage(int damageAmount)
    {
        health -= damageAmount;

        if (health <= 0)
        {
            playerDeath();
        }
    }

    private void playerDeath()
    {
        Application.LoadLevel(Application.loadedLevel);
        //throw new System.NotImplementedException();
    }
}
